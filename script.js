/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/ 

// 1)

const btn = document.querySelector('.btn');
const div = document.querySelector('.div')

let setTimeoutID = null;

btn.addEventListener('click', () => {
    if(setTimeoutID) return;
    setTimeoutID = setTimeout(() => {
        div.innerHTML = 'Операція виконана успішно!'
    }, 3000)
})

// 2)

const timer = document.querySelector('.timer');

let setIntervalID = null;
let number = 11;

window.onload = () => {
    setIntervalID = setInterval(() => {
        number--;
        timer.innerHTML = `${number}`;
        if(number === 0) {
            clearInterval(setIntervalID);
            timer.innerHTML = 'Зворотиній відлік завершено';
        }
    }, 1100)
    
}